import { parseISO, format } from 'date-fns'

// date component
export default function FormattedDate({ dateString }) {
  const date = parseISO(dateString)
  return <time dateTime={dateString}>{format(date, 'dd.MM.yyyy')}</time>
}

export function GetCurrentYear() {
  return format(new Date(), 'yyyy')
}

export function PlainTextDate(dateString) {
  const date = parseISO(dateString)
  return format(date, 'dd.MM.yyyy')
}
