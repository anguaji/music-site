import styles from './layout.module.scss'
import utilStyles from '../styles/utils.module.scss'
import { siteName, siteTag } from './layout'

export default function Hero({ home, heroProps }) {
  const { bgImage, headerText, tagText } = !!heroProps
    ? heroProps
    : {
        bgImage: '/images/bg/site-bg-1.jpg',
        headerText: siteName,
        tagText: siteTag,
      }

  return (
    <>
      <section
        className={styles.hero}
        style={{ backgroundImage: 'url(' + bgImage + ')' }}
      >
        {home ? (
          <>
            <h1
              className={
                (utilStyles.heading2Xl,
                utilStyles.marginTop1em,
                styles.container)
              }
            >
              {siteName} <span>{siteTag}</span>
            </h1>
          </>
        ) : (
          <>
            <h1
              className={
                (utilStyles.headingLg,
                utilStyles.marginTop1em,
                styles.container)
              }
            >
              {headerText}

              <span>{tagText}</span>
            </h1>
          </>
        )}
      </section>
    </>
  )
}
