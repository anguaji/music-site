import Head from 'next/head'
import Link from 'next/link'

import Hero from './hero'
import NavBar from './navBar'

import { GetCurrentYear } from './date'

import styles from './layout.module.scss'
import GoogleAnalytics from './ga'

const siteTags = [
  'composer for hire',
  'new music, who dis?',
  'making beats',
  'musician at large',
  "or Beth Brennan, if we're being formal",
  'have guitar, will travel',
]
export const siteName = 'anguaji.music'
export const siteTag = siteTags[(siteTags.length * Math.random()) | 0]
export const siteTitle = siteName + ' - ' + siteTag
export const mainTitle = 'Beth Brennan - Composer For Hire'

export function Page({ children, home, heroProps }) {
  return (
    <>
      <Head>
        <meta
          name="description"
          content="Composer For Hire North East England, UK"
        />

        {/* Twitter Card Meta */}
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@anguaji" />
        <meta
          name="twitter:title"
          content="Anguaji Music | Albums On Bandcamp And Whatnot"
        />
        <meta
          name="twitter:description"
          content="More anguaji musicking this way >>"
        />
        <meta
          name="twitter:image"
          content="http://www.anguaji.com/template/favicons/apple-touch-icon-180x180.png"
        />

        {/* Google fonts */}
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Montserrat:wght@700&display=swap"
          rel="stylesheet"
        />

        {/* OG Data */}
        <meta
          property="og:description"
          content="Anguaji Music - composer for hire"
        />
        <meta property="fb:admins" content="157183497629419" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.anguaji.com" />
        <meta property="og:image" content="/images/profile.png" />

        <meta
          name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0"
        />
      </Head>

      <NavBar />

      <Hero heroProps={heroProps} />

      <div className={home ? styles.indexContainer : styles.container}>
        <main>{children}</main>
        {!home && (
          <div className={styles.backToHome}>
            <Link href="/">
              <a>← Back to home</a>
            </Link>
          </div>
        )}
      </div>

      <footer>
        <p>All Content &copy; Beth Brennan {GetCurrentYear()}</p>
      </footer>

      <GoogleAnalytics />
    </>
  )
}
