import Link from 'next/link'
import { siteName } from './layout'

import styles from './layout.module.scss'

export default function NavBar() {
  const profilePic = '/images/profile.png'
  return (
    <>
      <header className={styles.navBar}>
        <nav className={styles.container}>
          <ul>
            <li className={styles.logo}>
              <Link href="/">
                <a>
                  <h1>{siteName}</h1>
                </a>
              </Link>
            </li>

            <li>
              <Link href="/about">
                <a>About</a>
              </Link>
            </li>

            <li>
              <Link href="http://anguaji.bandcamp.com">
                <a target="_blank" rel="noreferrer">
                  Bandcamp
                </a>
              </Link>
            </li>

            <li>
              <Link href="https://soundcloud.com/anguaji">
                <a target="_blank" rel="noreferrer">
                  Soundcloud
                </a>
              </Link>
            </li>

            <li>
              <Link href="http://twitter.com/anguaji">
                <a target="_blank" rel="noreferrer">
                  Twitter
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    </>
  )
}
