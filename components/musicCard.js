import styles from './musicCard.module.scss'

import Link from 'next/link'
import { PlainTextDate } from './date'

export default function MusicCard({ postData }) {
  const bgImage = '/images/bg/' + postData.image
  return (
    <Link href={`/music/${postData.id}`}>
      <a
        className={styles.musicCard}
        style={{ backgroundImage: 'url(' + bgImage + ')' }}
      >
        <h1>{postData.title}</h1>
        <h2>
          {postData.category} {postData.type} / {PlainTextDate(postData.date)}
        </h2>
      </a>
    </Link>
  )
}
