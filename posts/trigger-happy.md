---
title: 'Trigger, Happy'
date: '2015-10-01'
type: 'Release'
category: 'Bandcamp'
image: 'trigger-happy.jpg'
album:
  {
    tracks:
      [
        '1. Fun Time Girl [3:18]',
        '2. Shooting Stars [3:33]',
        '3. Sunday Mornings [3:48]',
        '4. Lost [3:22]',
        '5. Interlude [1:01]',
        '6. Pulled Under [3:25]',
        '7. Novembering [1:26]',
        '8. Without Feathers [3:12]',
        '9. All Things [3:04]',
        '10. Hey, Piñata! [5:42]',
      ],
    bandcampEmbed:
      '<iframe style="border: 0; width: 100%; height: 472px;" src="https://bandcamp.com/EmbeddedPlayer/album=2896227179/size=large/bgcol=333333/linkcol=4ec5ec/artwork=none/transparent=true/" seamless><a href="https://anguaji.bandcamp.com/album/trigger-happy-2">Trigger, Happy by Anguaji Music</a></iframe>
      ',
    artwork: 'trigger-happy-cover.jpg',
  }
---

Started in November 2013, the [demos](https://anguaji.bandcamp.com/album/trigger-happy-demos) for Trigger, Happy were originally bashed out for the [NaSoAlMo](NaSoAlMo) album-in-a-month project. They became a half-telling of my life at the time.

In 2014, I was approached by the netlabel [Relaxed Machinery](http://www.relaxedmachinery.com/) to release the finished tracks. Over the late summer of 2015, I revisited the original sketches and set about finishing and refining the music to give the work closure. What I didn't predict was that there'd be such a strong sense of the in-between time, the events of 2013-2015, imprinted on the new music. Those two years have really made a mark on these songs.

Happily, it's still a joyful jumble of up-and-down tracks that are not too far removed from my original intentions. They're a little quieter, a little louder, a little tighter, a little softer. I hope you enjoy them.

You can listen here, or grab the full release at [http://anguaji.bandcamp.com/](https://anguaji.bandcamp.com/album/trigger-happy-2)
