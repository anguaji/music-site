---
title: 'Melva'
date: '2021-07-01'
type: 'Score'
category: 'Theatre + Digital'
image: 'melva-cover.jpeg'
---

<div><iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1235637715&color=%2300f1ff&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=false"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/anguaji" title="anguaji" target="_blank" style="color: #cccccc; text-decoration: none;">anguaji</a> · <a href="https://soundcloud.com/anguaji/feggis-intro" title="Feggis Intro" target="_blank" style="color: #cccccc; text-decoration: none;">Feggis Intro</a></div></div>

> "Brave isn't how you feel, it's what you do"

In 2021 I was proud to collaborate on a mid-pandemic, digital version of Melva, a play written for kids to help them understand and talk about mental health issues. I'd long been a fan of the project, but when the creators, [Mortal Fools](https://www.mortalfools.org.uk/) decided to say fiddle-de-dee to the pandemic and record the play for a digital package, I snapped up their request for some original compositions.

<div class="scInformation">
    <article>
            <iframe width="97%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1412627410&color=%23ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=true&show_reposts=false&show_teaser=false&visual=true"></iframe>
        </article>
        <article>
            <strong>Tracks:</strong>
            <br /><br />
            1. Melva Theme.<br />
            2. Gideon.<br />
            3. Melva Wakes Up<br />
            4. Feggis Intro.<br />
        </article>
</div>

To find out more about Melva, visit: [The Official Melva Site](https://melva.org.uk/)
