---
title: 'Intergalactic'
date: '2022-03-16'
type: 'Score'
category: 'Theatre'
image: 'intergalactic.jpeg'
---

<div><iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1235632168&color=%2300f1ff&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=false"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/anguaji" title="anguaji" target="_blank" style="color: #cccccc; text-decoration: none;">anguaji</a> · <a href="https://soundcloud.com/anguaji/intergalactic-do-you-trust-me" title="Intergalactic: Do You Trust Me" target="_blank" style="color: #cccccc; text-decoration: none;">Intergalactic: Do You Trust Me</a></div></div>

> "You and me, we’re nothing like each other. Trust me."

> "Sometimes I think we might be, a bit. Quite a bit, actually."

I was commissioned in early 2022 to produce some music for the marvellous play Intergalactic - a beautiful, witty story of two boys finding unusual common ground. The show played from 16.3.22 - 19.3.22 and I was very happy to have helped craft some magic moments! 🌕

<div class="scInformation">
    <article>
            <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1412620726&color=%2300f1ff&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=false&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/anguaji" title="anguaji" target="_blank" style="color: #cccccc; text-decoration: none;">anguaji</a> · <a href="https://soundcloud.com/anguaji/sets/intergalactic-petals-constellations" title="Intergalactic (Petals &amp; Constellations)" target="_blank" style="color: #cccccc; text-decoration: none;">Intergalactic (Petals &amp; Constellations)</a></div>
        </article>
        <article>
            <strong>Tracks:</strong>
            <br /><br />
            1. Do You Trust Me?<br />
            2. Moody Sound Test Demo - one of the original tracks I threw together for the director whilst trying to establish a sound for the piece. This would eventually become Hassan's "breakdown" score.<br />
            3. Hopeful Guitar<br />
        </article>
</div>

## Reviews and kind words

⭐️⭐️⭐️⭐️⭐️ 'an impressive debut by any standards...The script is genuinely funny as well as touching...The two actors give heartfelt and skilled performances...The play definitely deserves to be more widely seen.' [The Reviews Hub](https://www.thereviewshub.com/intergalactic-petals-and-constellations-live-theatre-newcastle-upon-tyne/)

Find out more here: [Live Theatre Link](https://www.live.org.uk/index.php/whats-on/intergalactic-petals-and-constellations)
