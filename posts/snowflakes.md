---
title: 'The Terminal Velocity Of Snowflakes'
date: '2016-11-01'
type: 'Score'
category: 'Theatre'
image: 'snowflakes-cover.jpg'
---

<div><iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/661065590&color=%23ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=true&show_reposts=false&show_teaser=false"></iframe>
    </div>

> Did you know, no two snowflakes are ever alike?

Once [The Savage](http://www.anguaji.com/music/the-savage) had wrapped at the Live Theatre, I was lucky enough to move on to the full-length version of [Nina Berry](https://twitter.com/ninajayneberry)'s beguiling [The Terminal Velocity Of Snowflakes](https://www.live.org.uk/whats-on/terminal-velocity-snowflakes).

_Those following my [SoundCloud](https://soundcloud.com/anguaji) may be familiar with the title. Back in 2015, "Snowflakes" was a short 20 minute piece that was part of the Live Lab's Christmas Adventures anthology. I helped provide some music for the short and the track [Will I Figure It Out](https://soundcloud.com/anguaji/will-i-ever-figure-it-out?in=anguaji/sets/twenty-fifteen) was one of the original songs produced for this._

For the full-length piece, we decided to throw out all the previous music and start again with the fresh text. At times playful, innocent, and in other moments heart-wrenching and seemingly bleak, the play had so many stand-out moments that would need some score to match.

> I really got to work through some emotions during the two runs; it was like deep therapy.

The marvellously talented cast handled the long music cues like champions. The biggest technical challenge was probably the last monologue scene - clocking in around 6 minutes of tightly-underscored monologue. But once the timings had been nailed, and the three piano pieces synced up, the scene would flow perfectly - and would usually produce some sniffles from the audience in the silence that followed.

There was so much underscore in these two runs, I've struggled to share some of the pieces - underscore with no context can be three minutes of ambient followed by one minute of melody and that's hard to show off. But here's some curated pieces that I wanted to share with the world:

<div class="scInformation">
    <article>
            <iframe width="97%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/841141232&color=%23ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=true&show_reposts=false&show_teaser=false&visual=true"></iframe>
        </article>
        <article>
            <strong>Tracks:</strong>
            <br /><br />
            1. Sledging - a magic moment in snowy Heaton Park that in situ was preceded by about 3 minutes of ambient tinkles. I'm famous for my ambient tinkles.<br /><br />
            2. Will I Figure It All Out? - the show-stopping <em>all is lost</em> moment. The challenge for this piece was the length of the dual monologues preceeding the heavily timed "whoosh" moment. I like throwing ear-catcher sounds for actors to listen for whilst they're on stage so they know where they're at. So there's possibly more tiny loops added in the production of this track than I'd use for a film version of the same scene.<br /><br />
            3. Dreamy Transition - a small moment added in the 2017 run. The piece was arranged in the rehearsal room with the choreographer working with the actors, and was such a satisfying and tender moment once it was on stage.<br /><br />
            4. The Kiss - the reprisal of the snowflakes tinkles, but with added energy.<br /><br />
            5. Last Scene - one long piano run, split into three parts for timing purposes. The challenge was creating a piece that was infused with grief, but within 6 minutes could be strident and hopeful. The melody was also a theme throughout the rest of the play for various moments.
        </article>
</div>

## Reviews And Kind Words

⭐️⭐️⭐️⭐️⭐️ "An essential north-east masterpiece that deserves acclaim and adulation for all involved." Once Upon A Tyne

⭐️⭐️⭐️⭐️ "an assured debut...This is a confident production, which above all, marks Berry out as a writer with an exciting future ahead.'" The Stage

"evokes powerful human emotion...terrific performances...sensitive direction" British Theatre Guide

"a memorable success from all angles of production and creativity" Northern Lights
