---
title: 'Red Is The New Blue'
date: '2015-03-01'
type: 'Score'
category: 'Theatre'
image: 'mars-landscape.jpg'
---

In Summer 2015, I worked with the Live Theatre in Newcastle to compose some original music for their Live Lab production of Red Is The New Blue. The extremely talented chaps behind RITNB conceived a play that was a mix of performance poetry and drama centered around a one way mission to Mars.

Three members of the public are picked to be the first tv-syndicated crew in space, sent hurtling to Mars - and then what?

Trepidation, longing, reflection, solar flares, FIFA. You know, space stuff.

The soundtrack brief was a very specific retro-synth sound coupled with high emotion. I made the whole lot in three weeks. I loved working for and in the theatre space, and I enjoyed the music I made so much that I'm going to release some tidied and human-friendly versions (the original score was 43 minutes of original music, but this release will be pared down to under half an hour).

<div markdown="1">
    <iframe width="100%" style = "display: block; margin: 0 auto; width: 100%;" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/239953899&color=ff5500&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false"></iframe><br /><a href = "https://soundcloud.com/anguaji/mars-spinning-recut-1">https://soundcloud.com/anguaji/mars-spinning-recut-1</a>
</div>

Currently, I'm tidying some extra lead lines that weren't included in the stage version, and then you'll hear about it when it's out. If you want to keep informed about upcoming releases, pop yourself on my mailing list - I'll never spam you and MailChimp is super secure.
