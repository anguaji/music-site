---
title: 'The Savage'
date: '2016-06-30'
type: 'Score'
category: 'Theatre'
image: 'savage-dean.jpg'
---

<div><iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/272590479&color=%23ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=true&show_reposts=false&show_teaser=false"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/anguaji" title="anguaji" target="_blank" style="color: #cccccc; text-decoration: none;">anguaji</a> · <a href="https://soundcloud.com/anguaji/sad-home" title="Sad Home" target="_blank" style="color: #cccccc; text-decoration: none;">Sad Home</a></div></div>

> "The score by Northumbrian traditional musician Beth Brennan may be the most sinister application of folk music since The Wicker Man." The Guardian [Read More](https://www.theguardian.com/stage/2016/jul/07/the-savage-live-theatre-newcastle-review)

2016 was a big year for Anguaji Music!

In spring 2016 I was commissioned by the Live Theatre to work on their summer event [The Savage](http://www.live.org.uk/whats-on-book/the-savage) - a dramatisation of [the book by David Almond](https://www.amazon.co.uk/Savage-David-Almond/dp/1406319856) (known for [Skellig](https://en.wikipedia.org/wiki/Skellig))

Cue a lot of recording, rehearsing, fretting, mixing, harmonising, and travelling. What a wonderful few months. The director (Max Roberts) and all the production staff were great to work with, and the cast (Dean Bone, Kate Okello, Dani Arlington and Adam Welsh) were all super lovely, and took to the numbers like a dream. Oh yeah, it turned into half a musical. Did I metion that? It's like a musical/fantastic tale/philosophical musing hybrid. The choreographer (Lee Proud) put the most amazing dance sequences together. To my music! I couldn't quite get over that. Even watching it for the bazillionth time, I swelled with pride when they launched into a song.

The play itself was a wonderfully dark piece for young adults, although it would appeal to all ages. The subject matter was certainly not childish.

So, there's a lot of music in a play like this. But as you can imagine, song backings without cast recordings are a bit dull, so here's a selection of some of the oodles of underscore instead:

<div class="scInformation">
    <article>
            <iframe width="97%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/239357703&color=ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=true&show_reposts=false"></iframe>   
        </article>
        <article>
            <strong>Tracks:</strong><br /><br />
            1. Blue's Theme<br />
            2. Sad Home<br />
            3. Dad Theme (demo with synth)<br />
            4. River Theme (demo)<br />
            5. Hopper Invades Blue's Dream<br />
            6. You're Scaring Me<br />
            7. Blue Writes More Savage<br />
            8. The Savage Heads To Town (demo)<br />
            9. Resolution Blue's Theme            
        </article>
</div>

The show ran from 30th June 2016 - 23 July 2016 and you can find the original Live Theatre page here: [https://www.live.org.uk/whats-on-book/the-savage](https://www.live.org.uk/whats-on-book/the-savage)
