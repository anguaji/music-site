---
title: 'Trigger, Happy [NASOALMO]'
date: '2013-11-01'
type: 'Release'
category: 'Bandcamp'
image: 'maschine-pic-min.jpg'
album:
  {
    tracks:
      [
        '1. Fun Time Girl [3:12]',
        '2. Shooting Stars [3:34]',
        '3. Sunday Mornings [3:46]',
        '4. Lost And [2:35]',
        '5. ... [1:11]',
        '6. Pulled Under [2:55]',
        '7. Without Feathers [2:37]',
        '8. Hey, Piñata! [2:22]',
        '9. All Things [2:21]',
        '10. Novembering [1:32]',
      ],
    bandcampEmbed:
      '<iframe style="border: 0; width: 100%; height: 472px;" src="https://bandcamp.com/EmbeddedPlayer/album=2672073635/size=large/bgcol=333333/linkcol=4ec5ec/artwork=none/transparent=true/" seamless><a href="https://anguaji.bandcamp.com/album/trigger-happy-demos">Trigger, Happy { demos } by Anguaji Music</a></iframe>
      ',
    artwork: 'trig-demos.jpg',
  }
---

> "Life is like stepping into a boat that is about to sail out to the sea and sink." ~Zen Proverb

**Trigger, Happy: a portrait of the artist as a young piñata**

_Foreword:_ In the last week of October 2013, I was idling on Twitter with pal @Jkn. We got to talking about life, coffee, dogs, and a project to write and record a whole album in a month. I mean, a whole _album_ in a _month_. Holy hell-crackers did that sound like an interesting proposition, and with right speed did I jump verily on that shizzle and never looked back.

_Artist Note:_ "Pal Jkn" is John, from the netlabel Relaxed Machinery. rM went on to release Trigger, Happy in October 2015 - you can see the release page for the finished article here. It's funny how life works out, isn't it?
