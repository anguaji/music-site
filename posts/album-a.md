---
title: 'Album A'
date: '2012-01-01'
type: 'Release'
category: 'Bandcamp'
image: 'album-a.jpg'
album:
  {
    tracks:
      [
        '1. Studio Afternoon [5:22]',
        '2. Invisible City [4:20]',
        '3. On Meeting Again [2:18]',
        '4. From You (Remix for Synoiz) [5:19]',
        '5. Low Down [1:34]',
        '6. In A Chaotic Space [5:27]',
        '7. Photographs Of You [1:51]]',
      ],
    bandcampEmbed:
      '<iframe style="border: 0; width: 100%; height: 373px;" src="https://bandcamp.com/EmbeddedPlayer/album=613452985/size=large/bgcol=333333/linkcol=e32c14/artwork=none/transparent=true/" seamless><a href="https://anguaji.bandcamp.com/album/album-a">Album A by Anguaji Music</a></iframe>
      ',
    artwork: 'album-a.jpg',
  }
---

Album A is the first ever release of finished, polished and worn-in music tracks from Anguaji Music.

They were composed and recorded between 2009 and 2011, and were scrubbed up, polished and sent on their way in Spring 2012.

_All songs written and recorded by Anguaji Music in or about the North of England. Tinkered with and polished by Deon Vozov in or about LA, which is not the North of England._
