# WHAT IS THE ANGUAJI?

Just a nice flat site that reads markdown and spits it out cleanly using NextJS. Also included the ability to add raw html to the markdown for the Soundcloud embeds.

## How Do I Do A Run A Do?

`yarn install`

`yarn dev`

## How Be This Made?

Created with the following command:

`npx create-next-app nextjs-blog --use-npm --example "https://github.com/vercel/next-learn/tree/master/basics/learn-starter"`
