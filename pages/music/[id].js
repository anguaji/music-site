import Head from 'next/head'

import { Page } from '../../components/layout'
import FormattedDate, { PlainTextDate } from '../../components/date'

import { getAllPostsIds, getPostData } from '../../lib/posts'

import styles from './posts.module.scss'
import utilStyles from '../../styles/utils.module.scss'
import Image from 'next/image'

export default function Post({ postData }) {
  const bgImage = !!postData.image ? '/images/bg/' + postData.image : null

  const heroProps = {
    bgImage: bgImage,
    headerText: postData.title,
    tagText: `${postData.category} ${postData.type} / ${PlainTextDate(
      postData.date
    )}`,
  }

  const isAlbum = !!postData.album

  return (
    <Page heroProps={heroProps}>
      <Head>
        <title>{postData.title} - Beth Brennan, composer for hire</title>
      </Head>
      <article>
        {/*<h2 className={utilStyles.headingXl}>{postData.title}</h2>*/}
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
        <div className={utilStyles.lightText}>
          Release date: {PlainTextDate(postData.date)}
        </div>
      </article>

      {isAlbum ? (
        <section className={styles.albumInformation}>
          <article>
            <div className={styles.albumArt}>
              <Image
                src={'/images/bg/' + postData.album.artwork}
                width={500}
                height={500}
                layout="responsive"
              />
            </div>
          </article>

          <article
            className={styles.bandcampEmbed}
            dangerouslySetInnerHTML={{ __html: postData.album.bandcampEmbed }}
          ></article>
        </section>
      ) : null}
    </Page>
  )
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData,
    },
  }
}

export async function getStaticPaths() {
  const paths = getAllPostsIds()

  return {
    paths,
    fallback: false,
  }
}
