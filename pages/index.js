import Head from 'next/head'
import { mainTitle, Page } from '../components/layout'
import { getSortedPostsData } from '../lib/posts'
import musicCardStyles from '../components/musicCard.module.scss'
import MusicCard from '../components/musicCard'
import styles from '../components/layout.module.scss'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData,
    },
  }
}

export default function Home({ allPostsData }) {
  return (
    <Page home>
      <Head>
        <title>{mainTitle}</title>
      </Head>
      <section>
        <h2 className={styles.indexHeader}>Recent Work</h2>
        <section className={musicCardStyles.musicCardHolder}>
          {allPostsData.map((post, id) => {
            return <MusicCard key={id} postData={post} />
          })}
        </section>
      </section>
    </Page>
  )
}
