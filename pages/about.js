import Head from 'next/head'
import { mainTitle, Page } from '../components/layout'
import utilStyles from '../styles/utils.module.scss'
import { getSortedPostsData } from '../lib/posts'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData,
    },
  }
}

export default function About({ allPostsData }) {
  const heroProps = {
    bgImage: '/images/bg/wallpaper-3.jpg',
    tagText: 'A little bit about Beth',
    headerText: 'What is the Anguaji?',
  }

  return (
    <Page heroProps={heroProps}>
      <Head>
        <title>{mainTitle} - About Beth</title>
      </Head>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Who is this Beth Brennan?</h2>
        <blockquote>
          Just a girl, standing in front of a copy of Ableton, making some
          music, wishing she had a chair to sit in because her back's hurting.
        </blockquote>
      </section>
    </Page>
  )
}
